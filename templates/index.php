<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<title>Social - Animale</title>
	<link rel="icon" href="https://www.animale.com.br/arquivos/favicon16.png?v=636849819002170000" type="image/png" sizes="16x16" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://www.felipedesenvolvimento.com/gruposoma/animale-main.min.css" />
</head>
<body>

	<div class="topbar desktop">
		<p><strong>VENDEDOR ONLINE:</strong> REALIZE UMA COMPRA PERSONALIZADA COM DICAS EXCLUSIVAS DOS NOSSOS VENDEDORES <a href="https://www.animale.com.br">SAIBA MAIS</a></p>
	</div>

	<div class="header__mobile mobile">
		<span class="header__mobile-hamburger"></span>
		<a href="https://www.animale.com.br/"><img src="https://www.felipedesenvolvimento.com/gruposoma/img/animale-logo.png" /></a>
		<span class="header__mobile-search"></span>
		<a href="https://www.animale.com.br/checkout#/cart"><span class="header__mobile-checkout"></span></a>
	</div>
	<div class="topbar mobile">
		<p><strong>VENDEDOR ONLINE</strong><a href="javascript:void(0)">X</a></p>
	</div>

	<div class="social">

		<div class="social__sidebar">
			<a href="https://www.animale.com.br/"><h1><img src="https://www.felipedesenvolvimento.com/gruposoma/img/animale-logo.png" /></h1></a>
			<div class="social__sidebar-search">
				<input type="text" placeholder="BUSCAR" />
				<a href="javascript:void(0)"></a>
			</div>
			<div class="social__sidebar-menu">
				<span class="close mobile">X</span>
				<span>
					<a href="https://www.animale.com.br/novidades?O=OrderByReleaseDateDESC">NOVIDADES</a>
					<a href="https://www.animale.com.br/colecao?O=OrderByScoreDESC">COLEÇÃO</a>
					<a href="https://www.animale.com.br/acessorios?O=OrderByReleaseDateDESC">ACESSÓRIOS</a>
					<a href="https://www.animale.com.br/intimates?O=OrderByReleaseDateDESC">INTIMATES</a>
					<a href="https://www.animale.com.br/sale?O=OrderByReleaseDateDESC">SALE</a>
				</span>
				<span>
					<a href="https://www.animale.com.br/animale-oro">JOIAS</a>
				</span>
				<span>
					<a href="https://www.animale.com.br/inside-animale">INSIDE</a>
					<a href="https://www.animale.com.br/editoriais/editorial?id=97f82856-cb4b-11e9-82cd-0a047770c8a2">NOIR</a>
				</span>
				<span>
					<a href="https://www.animale.com.br/nossas-lojas">LOJAS</a>
					<a href="https://www.animale.com.br/institucional/contato">CONTATO</a>
				</span>
			</div>
		</div>

		<div class="social__content">
			<div class="left">
				<div class="social__content-user">
					<span class="social__content-user--avatar">
						<span>
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/Avatar.png" />
						</span>
						<span>
							<strong>Camila Freire</strong>
							<p>Rio de Janeiro - RJ</p>
						</span>
					</span>
					<span class="social__content-user--posts">
						<strong>0</strong> look(s) postado(s)
					</span>
					<span class="social__content-user--menu">
						<a href="javascript:void(0)" class="active">Feed geral</a>
						<a href="javascript:void(0)">Meus posts</a>
					</span>
				</div>
			</div>
			<div class="center">
				<div class="social__content-new">
					<div class="social__content-new--top">
						<img src="https://www.felipedesenvolvimento.com/gruposoma/img/Avatar.png" />
						<p>Qual o seu look de agora?</p>
					</div>
					<form method="POST" enctype="multipart/form-data">
						<fieldset class="social__content-new--msg">
							<textarea placeholder="Digite uma descricão" name="description" maxlength="140"></textarea>
						</fieldset>
						<div class="social__content-new--options">
							<a href="javascript:void(0)">Adicionar foto</a>
							<a href="javascript:void(0)">Enviar</a>
								<input type="file" name="pic" accept="image/*" class="form-control" style="display:none">
						</div>
					</form>
				</div>

				<div class="social__content-posts">
					<span class="social__content-posts--filter">
						<span>FILTRAR POR CIDADE <i></i></span>
						<ul>
							<li id="all">Exibir tudo</li>
							<li id="rj">Rio de Janeiro</li>
							<li id="nt">Niterói</li>
							<li id="bz">Búzios</li>
						</ul>
					</span>

					<?php
					if(isset($_FILES['pic']) && isset($_REQUEST['description']))
					{
					   $ext = strtolower(substr($_FILES['pic']['name'],-4)); //Pegando extensão do arquivo
					   $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
					   $dir = './imagens/'; //Diretório para uploads
					
					   move_uploaded_file($_FILES['pic']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo

					   echo '
					   <div class="post me rj">
					   <div class="post__user">
							<span>
								<img src="https://www.felipedesenvolvimento.com/gruposoma/img/Avatar.png" />
							</span>
							<span>
								<strong>Camila Freire</strong>
								<p>Rio de Janeiro - RJ</p>
							</span>
						</div>
					   <div class="post__description">
					   	'.$_REQUEST['description'].'
					   </div>
					   <div class="post__image">
					   	<img src="./imagens/'.$new_name.'" />
					   </div>
					   <div class="post__options">
							<a class="post__options-like"></a>
							<a class="post__options-comment"></a>
							<span><p><strong>0</strong> curtidas</p> <p><strong>0</strong> comentário(s)</p></span>
						</div>
					   <div class="post__comments"></div>
					   </div>
					   ';
					} ?>

					<div class="post nt">
						<div class="post__user">
							<span>
								<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar1.png" />
							</span>
							<span>
								<strong>Yasmin Gouveia</strong>
								<p>Niterói - RJ</p>
							</span>
						</div>
						<div class="post__description">
							Camisa de seda com toque cool com as listras coloridas
						</div>
						<div class="post__image">
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/post1.png" />
						</div>
						<div class="post__options">
							<a class="post__options-like"></a>
							<a class="post__options-comment"></a>
							<span><p><strong>0</strong> curtidas</p> <p><strong>1</strong> comentário(s)</p></span>
						</div>
						<div class="post__comments">
							<div class="post__comments-comment">
								<span>
									<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar2.png" />
									<strong>Ana Beatriz</strong>
								</span>
								<p>
									Que lindo
								</p>
							</div>
						</div>
					</div>

					<div class="post bz">
						<div class="post__user">
							<span>
								<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar2.png" />
							</span>
							<span>
								<strong>Ana Beatriz</strong>
								<p>Búzios - RJ</p>
							</span>
						</div>
						<div class="post__description">
							Vestido mini em jersey! :)
						</div>
						<div class="post__image">
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/post2.png" />
						</div>
						<div class="post__options">
							<a class="post__options-like"></a>
							<a class="post__options-comment"></a>
							<span><p><strong>2</strong> curtidas</p> <p><strong>2</strong> comentário(s)</p></span>
						</div>
						<div class="post__comments">
							<div class="post__comments-comment">
								<span>
									<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar1.png" />
									<strong>Yasmin Gouveia</strong>
								</span>
								<p>
									Lindo demais!!!
								</p>
							</div>
							<div class="post__comments-comment">
								<span>
									<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar2.png" />
									<strong>Ana Beatriz</strong>
								</span>
								<p>
									Obrigada =]
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="right">
				<div class="social__content-members">
					<h2>Novos membros</h2>
					<ul>
						<li>
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar1.png" />
							<strong>Yasmin Gouveia</strong>
						</li>
						<li>
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/avatar2.png" />
							<strong>Ana Beatriz</strong>
						</li>
						<li>
							<img src="https://www.felipedesenvolvimento.com/gruposoma/img/Avatar.png" />
							<strong>Camila Freire (Você)</strong>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>

	<div id="overlay"></div>

	<footer>
		<div></div>
		<p>RBX RIO COMÉRCIO DE ROUPAS SA. ESTRADA DOS BANDEIRANTES, 1.700 - GALPÃO 03, ARMAZÉM 104 - TAQUARA, RIO DE JANEIRO, RJ - CEP: 22775-109. CNPJ: 10.285.590/0002-80.</p>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://www.felipedesenvolvimento.com/gruposoma/animale-social.min.js"></script>
</body>
</html>