$(document).ready(function(){
	$(".social__content-new--options > a").click(function(){
		if($(this).index() == 0){
			if($("form input").val() == ""){
				$("form input").trigger("click");
			}
		}
		else{
			$("form").submit();
		}
	});

	$("form input").change(function(){
		$("form a:eq(0)").addClass("active").text("Foto adicionada");
	});

	$(".social__content-user--posts strong").text($(".post.me").length);

	$(".social__content-user--menu > a").click(function(){
		$(".social__content-user--menu > a").removeClass("active");
		$(this).addClass("active");
		$(".post").show();
		if($(this).index() == 1){
			$(".post:not(.me)").hide();
		}
	});

	$(".social__content-posts--filter > span").hover(function(){
		$(".social__content-posts--filter ul").addClass("active");
		$(".social__content-posts--filter ul li").click(function(){
			$(".social__content-user--menu > a:eq(0)").trigger("click");
			$(".post").show();
			$(".social__content-posts--filter ul").removeClass("active");
			var filter = $(this).attr("id");
			if(filter != "all"){
				$(".post:not(."+filter+")").hide();
			}
		});
	});

	$(".social__content-posts--filter ul").mouseleave(function(){
		$(this).removeClass("active");
	});

	$(".post__options-like").click(function(){
		$(this).toggleClass("active");

		if($(this).hasClass("active")){
			var nComments = parseInt($(this).parents(".post").find(".post__options > span > p:eq(0) > strong").text()) + 1;
		}
		else{
			var nComments = parseInt($(this).parents(".post").find(".post__options > span > p:eq(0) > strong").text()) - 1;
		}
		
		$(this).parents(".post").find(".post__options > span > p:eq(0) > strong").html(nComments);

	});

	$(".post__options-comment").click(function(){
		var comment = '<div class="post__comments-comment">\
								<span>\
									<img src="https://www.felipedesenvolvimento.com/gruposoma/img/Avatar.png">\
									<strong>Camila Freire</strong>\
								</span>\
								<input type="text" placeholder="Novo comentário" maxlength="30" />\
								<a href="javascript:void(0)" id="insertComment">OK</a>\
							</div>';
		if(!$(this).parents(".post").find(".post__comments input").length){
			$(this).parents(".post").find(".post__comments").append(comment);
			$(this).parents(".post").find(".post__comments input").focus();
		}

		$("#insertComment").click(function(){
			var commentVal = '<p>'+$(this).parents(".post__comments-comment").find("input").val()+'</p>';
			$(this).parents(".post__comments-comment").find("input").remove();
			$(commentVal).insertAfter($(this).parents(".post__comments-comment").find("span"));
			
			var nComments = parseInt($(this).parents(".post").find(".post__options > span > p:eq(1) > strong").text()) + 1;
			$(this).parents(".post").find(".post__options > span > p:eq(1) > strong").html(nComments);

			$(this).remove();
		});
	});

	$(".social__sidebar-search > a").click(function(){
		var searchTerm = $(".social__sidebar-search > input").val();
		window.location = "https://www.animale.com.br/"+searchTerm;
	});

	if(window.innerWidth < 1300){
		$(".header__mobile-hamburger").click(function(){
			$(".social__sidebar-menu").addClass("active");
		});

		$(".social__sidebar-menu .close").click(function(){
			$(".social__sidebar-menu").removeClass("active");
		});

		$(".topbar.mobile a").click(function(){
			$(".topbar").remove();
		});

		$(".header__mobile-search").click(function(){
			$(".social__sidebar-search,#overlay").addClass("active");

			$("#overlay").click(function(){
				$(".social__sidebar-search,#overlay").removeClass("active");
			});
		});

	}

});